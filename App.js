import React, { useEffect, useState, } from 'react';
import {
  FlatList,
  ScrollView,
  TextInput,
  StyleSheet,
  Text,
  View,
} from 'react-native';

import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";

var list = [
  {
    userId: 1,
    id: 1,
    title: "delectus aut autem",
    completed: false
  },
  {
    userId: 1,
    id: 2,
    title: "quis ut nam facilis et officia qui",
    completed: false
  },
  {
    userId: 1,
    id: 3,
    title: "fugiat veniam minus",
    completed: false
  },
  {
    userId: 1,
    id: 4,
    title: "et porro tempora",
    completed: true
  },
  {
    userId: 1,
    id: 5,
    title: "laboriosam mollitia et enim quasi adipisci quia provident illum",
    completed: false
  },
  {
    userId: 1,
    id: 6,
    title: "qui ullam ratione quibusdam voluptatem quia omnis",
    completed: false
  },
  {
    userId: 1,
    id: 7,
    title: "illo expedita consequatur quia in",
    completed: false
  },
  {
    userId: 1,
    id: 8,
    title: "quo adipisci enim quam ut ab",
    completed: true
  },
  {
    userId: 1,
    id: 9,
    title: "molestiae perspiciatis ipsa",
    completed: false
  },
  {
    userId: 1,
    id: 10,
    title: "illo est ratione doloremque quia maiores aut",
    completed: true
  },
  {
    userId: 1,
    id: 11,
    title: "vero rerum temporibus dolor",
    completed: true
  },
  {
    userId: 1,
    id: 12,
    title: "ipsa repellendus fugit nisi",
    completed: true
  },
  {
    userId: 1,
    id: 13,
    title: "et doloremque nulla",
    completed: false
  },
  {
    userId: 1,
    id: 14,
    title: "repellendus sunt dolores architecto voluptatum",
    completed: true
  },
  {
    userId: 1,
    id: 15,
    title: "ab voluptatum amet voluptas",
    completed: true
  },
  {
    userId: 1,
    id: 16,
    title: "accusamus eos facilis sint et aut voluptatem",
    completed: true
  },
  {
    userId: 1,
    id: 17,
    title: "quo laboriosam deleniti aut qui",
    completed: true
  },
  {
    userId: 1,
    id: 18,
    title: "dolorum est consequatur ea mollitia in culpa",
    completed: false
  },
  {
    userId: 1,
    id: 19,
    title: "molestiae ipsa aut voluptatibus pariatur dolor nihil",
    completed: true
  },
  {
    userId: 1,
    id: 20,
    title: "ullam nobis libero sapiente ad optio sint",
    completed: true
  },
];

const ToDoItem = (props) => {
  const todo = props.todo;
  const completedIcon = todo.completed ? "check-circle" : "circle";
  const completedColor = todo.completed ? "green" : "red";
  let completedStyle = todo.completed ? styles.completedStyle : styles.nonCompletedStyle;

  return (
    <View style={styles.toDoItemStyle}>
      <View style={styles.toDoListTile}>
        <FontAwesomeIcon
          name={completedIcon}
          size={25}
          color={completedColor}
          style={styles.rightIcon}
          onPress={() => { props.updateTodo(todo); }}
        />
        <Text style={completedStyle}>{todo.title}</Text>
      </View>
      <FontAwesomeIcon
        name="trash"
        size={24}
        color="red"
        onPress={() => { props.deleteTodo(todo); }}
      />
    </View>
  );
};

const App = () => {
  const [todoList, setTodoList] = useState([]);
  const [todoText, setTodoText] = useState("");

  useEffect(() => {
    setTodoList(list);
  }, [todoList]);

  const displayToDoList = () => {
    if (todoList.length === 0) {
      return (
        <View style={styles.noToDoFound}>
          <Text>No TO-DOs left</Text>
        </View>
      );
    }
    
    return (
      <FlatList
        data={todoList}
        renderItem={displaySingleToDo}
        keyExtractor={(t) => t.id}
        showsVerticalScrollIndicator={false}
      />
    );
  };

  const displaySingleToDo = (toDo) => {
    const t = toDo.item;
    return (
      <ToDoItem
        todo={t}
        key={t.id}
        updateTodo={updateTodo}
        deleteTodo={deleteTodo} />
    );
  };

  const insertToDo = () => {
    if (!todoText) return;

    const toDoObj = {
      userId: 1,
      id: todoList.length + 1,
      title: todoText,
      completed: false
    };

    todoList.push(toDoObj);
    setTodoList([...todoList]);
    setTodoText("");
  };

  const updateTodo = (pToDo) => {
    const idx = todoList.indexOf(pToDo);
    const todo = todoList[idx];
    todo.completed = !todo.completed;

    setTodoList([...todoList]);
  };

  const deleteTodo = (pToDo) => {
    const idx = todoList.indexOf(pToDo);
    todoList.splice(idx, 1);
    setTodoList([...todoList]);
  };

  return (
    <View style={styles.screen}>
      <Text style={styles.heading}>My To-Do List</Text>
      <View style={styles.addToDo}>
        <TextInput
          value={todoText}
          onChangeText={text => setTodoText(text)}
          placeholder="Add more to-dos"
        />
        <FontAwesomeIcon
          name="plus-square-o"
          size={25}
          color='black'
          style={styles.rightIcon}
          onPress={() => { insertToDo(); }}
        />
      </View>
      <View style={styles.listStyle}>
        {displayToDoList()}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    margin: 15,
  },
  heading: {
    fontSize: 20,
    fontWeight: '800',
  },
  addToDo: {
    marginTop: 12,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  listStyle: {
    flex: 1,
    margin: 10,
  },
  rightIcon: {
    marginRight: 18,
  },
  leftIcon: {},
  noToDoFound: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  toDoItemStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 55,
  },
  toDoListTile: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  completedStyle: {
    color: 'black',
    flexWrap: 'wrap',
    width: '80%',
    textDecorationLine: 'line-through',
  },
  nonCompletedStyle: {
    color: 'black',
    flexWrap: 'wrap',
    width: '80%',
  },
});

export default App;
